<?php

namespace Tests\Unit\App\Services;

use App\Services\WebcamsProvider;
use App\Webcam;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class WebcamsProviderTest extends TestCase
{
    /**
     * @var WebcamsProvider
     */
    private $webcamsProvider;

    public function setUp()
    {
        $this->webcamsProvider = new WebcamsProvider();
        parent::setUp();
    }

    public function testThatGetReturnAValidCollectionOfWebcams()
    {
        $result = $this->webcamsProvider->get();

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertInstanceOf(Webcam::class, $result->first());
    }

    public function testThatGetUseCache()
    {
        Cache::shouldReceive('remember')
            ->with('index-webcams', 15, \Closure::class)
            ->once()
            ->andReturn(collect([]));

        $result = $this->webcamsProvider->get();
    }
}
