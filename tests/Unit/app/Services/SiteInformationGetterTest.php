<?php

namespace Tests\Unit\App\Services;

use App\Services\SiteInformationGetter;
use App\Site;
use Illuminate\Support\Collection;
use Tests\TestCase;

class SiteInformationGetterTest extends TestCase
{
    /**
     * @var SiteInformationGetter
     */
    private $siteInformationGetter;

    public function setUp()
    {
        $this->siteInformationGetter = new SiteInformationGetter();
    }

    /**
     * @expectedException  \App\Exceptions\SiteNotFoundException
     */
    public function testThatGetReturnAndSiteNotFoundException()
    {
        $this->siteInformationGetter->get('random-domain.com');
    }

    public function testThatGetReturnTheSiteWeExpect()
    {
        $site = $this->siteInformationGetter->get('conejox.com');

        $this->assertEquals('conejox.com', $site->domain);
        $this->assertInstanceOf(Site::class, $site);
    }

    public function testThatAllMethodReturnThreeSites()
    {
        $result = $this->siteInformationGetter->all();

        $this->assertEquals(3, $result->count());
    }

    public function testThatAllMethodReturnACollection()
    {
        $result = $this->siteInformationGetter->all();

        $this->assertInstanceOf(Collection::class, $result);
    }
}
