const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .styles(['resources/css/babosas.css', 'resources/css/reset.css', 'resources/css/simple-container.css'], 'public/css/babosas.css')
    .styles(['resources/css/cerdas.css', 'resources/css/reset.css', 'resources/css/simple-container.css'], 'public/css/cerdas.css')
    .styles(['resources/css/conejox.css', 'resources/css/reset.css', 'resources/css/simple-container.css'], 'public/css/conejox.css')
    .scripts(['resources/js/app.js'], 'public/js/app.js');
