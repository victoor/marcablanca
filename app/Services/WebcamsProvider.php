<?php

namespace App\Services;

use App\Webcam;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class WebcamsProvider
{
    private $url = 'http://webcams.cumlouder.com/feed/webcams/online/61/1/';

    public function __construct($url = null)
    {
        if ($url !== null) {
            $this->url = $url;
        }
    }

    public function get(): Collection
    {
        // Reload webcams every 15 minutes
        $webcams = Cache::remember('index-webcams', 15, function () {
            return collect(json_decode(file_get_contents($this->url), TRUE))
                ->map(function (array $rawWebcame) {
                    return new Webcam($rawWebcame);
                });
        });

        return $webcams;
    }
}
