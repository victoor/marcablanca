<?php

namespace App\Services;

use App\Exceptions\SiteNotFoundException;
use App\Site;
use Illuminate\Support\Collection;

class SiteInformationGetter
{
    public function get(string $domain): Site
    {
        $site = $this->all()->where('domain', '=', $domain)->first();

        if ($site === null) {
            throw new SiteNotFoundException("No site found for domain: $domain");
        }

        return $site;
    }

    public function all(): Collection
    {
        return collect([
            new Site([
                "name" => "ConejoX",
                "domain" => "conejox.com",
                "webcameNatsTrackingCode" => "cxwMjYwNS4xLjI1LjQzLjAuMC4wLjAuMA",
                "cumlouderNatsTrackingCode" => "cxcMjYwNS4xLjIuMi4wLjAuMC4wLjA",
                "customCss" => "conejox.css",
                "gAnalyticsCode" => "UA-26020144-23",
            ]),
            new Site([
                "name" => "Cerdas",
                "domain" => "cerdas.com",
                "webcameNatsTrackingCode" => "cdwMjYwNS4xLjI1LjQzLjAuMC4wLjAuMA",
                "cumlouderNatsTrackingCode" => "cdcMjYwNS4xLjIuMi4wLjAuMC4wLjA",
                "customCss" => "cerdas.css",
                "gAnalyticsCode" => "UA-26020144-24",
            ]),
            new Site([
                "name" => "Babosas",
                "domain" => "babosas.biz",
                "webcameNatsTrackingCode" => "bbwMjYwNS4xLjI1LjQzLjAuMC4wLjAuMA",
                "cumlouderNatsTrackingCode" => "bbcMjYwNS4xLjIuMi4wLjAuMC4wLjA",
                "customCss" => "babosas.css",
                "gAnalyticsCode" => "UA-26020144-26",
            ])
        ]);
    }
}
