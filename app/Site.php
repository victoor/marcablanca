<?php

namespace App;

use Illuminate\Database\Eloquent\Concerns\HasAttributes;

class Site
{
    use HasAttributes;

    public function __construct(array $data)
    {
        $this->name = $data["name"];
        $this->domain = $data["domain"];
        $this->webcameNatsTrackingCode = $data["webcameNatsTrackingCode"];
        $this->cumlouderNatsTrackingCode = $data["cumlouderNatsTrackingCode"];
        $this->customCss = $data["customCss"];
        $this->gAnalyticsCode = $data["gAnalyticsCode"];
    }
}
