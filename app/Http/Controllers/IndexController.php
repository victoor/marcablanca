<?php

namespace App\Http\Controllers;

use App\Services\WebcamsProvider;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var WebcamsProvider
     */
    private $webcamsProvider;

    public function __construct(WebcamsProvider $webcamsProvider)
    {
        $this->webcamsProvider = $webcamsProvider;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // This could be paginated if we want to
        $webcams = $this->webcamsProvider->get()->forPage(1, 36);

        return view('index', [
            'webcams' => $webcams
        ]);
    }
}
