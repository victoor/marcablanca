<?php

namespace App\Http\Middleware;

use App\Services\SiteInformationGetter;
use Closure;

class CheckDomain
{
    /**
     * @var SiteInformationGetter
     */
    private $domainInformationGetter;

    public function __construct(SiteInformationGetter $domainInformationGetter)
    {
        $this->domainInformationGetter = $domainInformationGetter;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $site = $this->domainInformationGetter->get($request->getHost());
        $request->request->set('site', $site);

        return $next($request);
    }
}
