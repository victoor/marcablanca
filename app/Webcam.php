<?php

namespace App;

use Illuminate\Database\Eloquent\Concerns\HasAttributes;

class Webcam
{
    use HasAttributes;

    public function __construct(array $rawWebcame)
    {
        $this->wbmerTwitter = $rawWebcame["wbmerTwitter"];
        $this->wbmerId = $rawWebcame["wbmerId"];
        $this->wbmerNick = $rawWebcame["wbmerNick"];
        $this->wbmerPermalink = $rawWebcame["wbmerPermalink"];
        $this->wbmerOnline = $rawWebcame["wbmerOnline"];
        $this->wbmerBirthdate = $rawWebcame["wbmerBirthdate"];
        $this->wbmerHeight = $rawWebcame["wbmerHeight"];
        $this->wbmerCountry = $rawWebcame["wbmerCountry"];
        $this->wbmerSystem = $rawWebcame["wbmerSystem"];
        $this->wbmerSystemId = $rawWebcame["wbmerSystemId"];
        $this->wbmerThumb1 = $rawWebcame["wbmerThumb1"];
        $this->wbmerThumb2 = $rawWebcame["wbmerThumb2"];
        $this->wbmerThumb3 = $rawWebcame["wbmerThumb3"];
        $this->wbmerThumb4 = $rawWebcame["wbmerThumb4"];
        $this->wbmerActive = $rawWebcame["wbmerActive"];
        $this->wbmerLive = $rawWebcame["wbmerLive"];
        $this->wbmerNew = $rawWebcame["wbmerNew"];
        $this->wbmerBanned = $rawWebcame["wbmerBanned"];
        $this->wbmerRanking = $rawWebcame["wbmerRanking"];
        $this->wbmerLastLogin = $rawWebcame["wbmerLastLogin"];
        $this->wbmerRegister = $rawWebcame["wbmerRegister"];
        $this->wbmerPHd = $rawWebcame["wbmerPHd"];
        $this->wbmerPAudio = $rawWebcame["wbmerPAudio"];
        $this->wbmerDtRate = $rawWebcame["wbmerDtRate"];
        $this->wbmerEspSwf = $rawWebcame["wbmerEspSwf"];
        $this->wbmerVideos = $rawWebcame["wbmerVideos"];
        $this->wbmerPositionOnline = $rawWebcame["wbmerPositionOnline"];
        $this->wbmerPosition = $rawWebcame["wbmerPosition"];
        $this->languages = $rawWebcame["languages"];
    }
}
