jQuery(function ($) {
    $('div.chica a.link').click(function (e) {
        e.preventDefault();

        var height = $(window).innerHeight() * 0.8;
        var width = Math.min($(window).innerWidth() * 0.8, 980);

        $.modal('<iframe src="' + $(this).attr('href') + '" height="' + height + '" width="' + width + '" style="border:0">', {
            closeHTML:"",
            containerCss:{
                backgroundColor:"#fff",
                height: height + 3,
                padding: 10,
                width: width + 3
            },
            overlayClose:true
        });
    })
});
