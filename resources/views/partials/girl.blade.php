<div class="chica{{ !empty($classes) ? ' ' . $classes : '' }}">
    <a class="link" href="http://webcams.cumlouder.com/joinmb/cumlouder/{{ $webcam->wbmerPermalink }}/?nats={{ request('site')->webcameNatsTrackingCode }}"
       title="Ver a {{ $webcam->wbmerNick }} online">
        <span class="thumb">
            @if (!empty($classes) && strpos($classes, 'chica-grande') !== false)
                <img src="///w0.imgcm.com/modelos/{{ $webcam->wbmerThumb3 }}"
                     alt="{{ $webcam->wbmerNick }}"
                     title="{{ $webcam->wbmerNick }}" />
            @else
                <img src="///w0.imgcm.com/modelos/{{ $webcam->wbmerThumb2 }}"
                     alt="{{ $webcam->wbmerNick }}"
                     title="{{ $webcam->wbmerNick }}" />
            @endif
        </span>

        <span class="nombre-chica">
            @if ($webcam->wbmerOnline)
                <span class="ico-online"></span>
            @endif

            {{ $webcam->wbmerNick }}
        </span>

        <span id="favorito" class="ico-favorito" ></span>
    </a>
</div>
