@extends('layouts.main')

@section('content')
    <div class="listado-chicas">
        @foreach($webcams as $index => $webcam)
            @if ($index === 5 || $index === 29)
                @include('partials.girl', [
                    'webcam' => $webcam,
                    'classes' => 'chica-grande'
                ])
            @elseif ($index === 17)
                @include('partials.girl', [
                    'webcam' => $webcam,
                    'classes' => 'chica-grande grande-derecha'
                ])
            @else
                @include('partials.girl', ['webcam' => $webcam])
            @endif
        @endforeach

        <div class="clear"></div>

        <a class="btn-mas-modelos" href="#" title="Mostrar más modelos">Mostrar Más Modelos</a>
    </div>
    <!-- termina LISTADO DE CHICAS -->
@endsection
