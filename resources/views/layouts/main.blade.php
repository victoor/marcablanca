<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>{{ request('site')->name }}</title>

    <!-- Star CSS and Javascript -->
    <link rel="stylesheet" href="/css/{{ request('site')->customCss }}" type="text/css" media="screen,projection">
    <!-- end CSS and Javascript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/simplemodal/1.4.4/jquery.simplemodal.min.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>

    <!-- Google Analytics code: {{ request('site')->gAnalyticsCode }} -->
</head>

<body>

<div class="box-header">
    <div class="header">
        <h1 class="logo-sitio"><a href="#" title="{{ request('site')->name }}">{{ request('site')->name }}</a></h1>
        <div class="tit-webcams">Webcams</div>

        <div class="logo-cum"><a href="#" title="Cumlouder.com">Cumlouder.com</a></div>

        <div class="menu">
            <a href="#" title="Acceso a las Chicas en Directo">Acceso a las Chicas en Directo</a> <span>|</span>
            <a href="#" title="Acceso Miembros">Acceso Miembros</a> <span>|</span>
            <a href="#" title="Compra Créditos">Compra Créditos</a>
        </div>

        <div class="clear"></div>
    </div>
</div>
<!-- termina HEADER -->

@yield('content')

<div class="box-footer">
    <div class="menu">
        <a href="#" title="Acceso a las Chicas en Directo">Acceso a las Chicas en Directo</a> <span>|</span>
        <a href="#" title="Acceso Miembros">Acceso Miembros</a> <span>|</span>
        <a href="#" title="Compra Créditos">Compra Créditos</a>
    </div>
</div>
<!-- termina MENU FOOTER -->

<div class="box-copy">
    <div class="menu">
        <p>Copyright © WAMCash Spain Todos los derechos reservados <span>|</span> <a href="#" title="Webmasters">Webmasters</a> </p>
        <p>Contenido para adultos <span>|</span> Tienes que tener mas de 18 años para poder visitarlo. Todas las modelos de esta web son mayores de edad.</p>
    </div>
</div>
<!-- termina COPY -->

<div class="box-data">
    <div class="menu">
        <a href="#" title="Soporte Epoch">Soporte Epoch</a> <span>|</span>
        <a href="#" title="18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement">18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement</a> <span>|</span>
        <a href="#" title="Contacto">Contacto</a> <span>|</span>
        <a href="#" title="Please visit Epoch.com, our authorized sales agent">Please visit Epoch.com, our authorized sales agent</a>
    </div>
</div>
<!-- termina DATA -->

</body>
</html>
